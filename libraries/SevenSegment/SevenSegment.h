// ---------------------------------------------------------------------------
// SevenSegment Library - v1.0 - 10/27/2013
//
// AUTHOR/LICENSE:
// Created by Thaddeus Bort, Jr - thaddeusbort@gmail.com
// Copyright 2013 License: GNU GPL v3 http://www.gnu.org/licenses/gpl-3.0.html
//
// DISCLAIMER:
// This software is furnished "as is", without technical support, and with no 
// warranty, express or implied, as to its usefulness for any purpose.
//
// PURPOSE:
// Replacement to the standard tone library with the advantage of nearly twice
// the volume, higher frequencies (even if running at a lower clock speed),
// higher quality (less clicking), nearly 1.5k smaller compiled code and less
// stress on the speaker. Disadvantages are that it must use certain pins and
// it uses two pins instead of one. But, if you're flexible with your pin
// choices, this is a great upgrade. It also uses timer 1 instead of timer 2,
// which may free up a conflict you have with the tone library. It exclusively 
// uses port registers for the fastest and smallest code possible.
//
// USAGE:
// Connection is very similar to a piezo or standard speaker. Except, instead
// of connecting one speaker wire to ground you connect both speaker wires to
// Arduino pins. The pins you connect to are specific, as toneAC lets the
// ATmega microcontroller do all the pin timing and switching. This is
// important due to the high switching speed possible with toneAC and to make
// sure the pins are alyways perfectly out of phase with each other
// (push/pull). See the below CONNECTION section for which pins to use for
// different Arduinos. Just as usual when connecting a speaker, make sure you
// add an inline 100 ohm resistor between one of the pins and the speaker wire.
//
// CONNECTION:
//   Pins  9 & 10 - ATmega328, ATmega128, ATmega640, ATmega8, Uno, Leonardo, etc.
//   Pins 11 & 12 - ATmega2560/2561, ATmega1280/1281, Mega
//   Pins 12 & 13 - ATmega1284P, ATmega644
//   Pins 14 & 15 - Teensy 2.0
//   Pins 25 & 26 - Teensy++ 2.0
//
// SYNTAX:
//   toneAC( frequency [, volume [, length [, background ]]] ) - Play a note.
//     Parameters:
//       * frequency  - Play the specified frequency indefinitely, turn off with toneAC().
//       * volume     - [optional] Set a volume level. (default: 10, range: 0 to 10 [0 = off])
//       * length     - [optional] Set the length to play in milliseconds. (default: 0 [forever], range: 0 to 2^32-1)
//       * background - [optional] Play note in background or pause till finished? (default: false, values: true/false)
//   toneAC_RTTTL( rtttlSong [, volume [, background ]] ) - Play an RTTTL formatted song.
//     Parameters:
//       * rtttlSong  - Play a song with the notes and durations specified in the rtttl formatted char[]. Play once and turn off.
//       * volume     - [optional] Set a volume level. (default: 10, range: 0 to 10 [0 = off])
//       * background - [optional] Play song in background or pause application until finished? (default: false, value: true/false)
//   toneAC()    - Stop playing.
//   noToneAC()  - Same as toneAC().
//
// HISTORY:
// 10/27/2013 v1.0 - Initial release.
//
// ---------------------------------------------------------------------------

#ifndef SevenSegment_h
#define SevenSegment_h

#include <Arduino.h>

const byte segment_ascii[] = {
  0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F, // numerals 0-9 at corresponding indexes
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F, // numerals 0-9 at ascii locations
  0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 
  // A     B     C     D     E     F     G     H     I     J     K     L     M     N     O     P     Q     R     S     T     U     V     W     X     Y     Z
  0x77, 0x7F, 0x39, 0x5E, 0x79, 0x71, 0x3D, 0x76, 0x30, 0x1E, 0x76, 0x38, 0x15, 0x37, 0x3F, 0x73, 0x67, 0x33, 0x6D, 0x78, 0x3E, 0x3E, 0x2A, 0x76, 0x6E, 0x5B, // uppercase letters
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x5F, 0x7C, 0x58, 0x5E, 0x7B, 0x71, 0x6F, 0x74, 0x10, 0x0C, 0x76, 0x30, 0x14, 0x54, 0x5C, 0x73, 0x67, 0x50, 0x6D, 0x78, 0x1C, 0x1C, 0x14, 0x76, 0x6E, 0x5B, // lowercase letters
  0x00, 0x00, 0x00, 0x00, 0x00
 };

const byte DIGIT_1 = 0x01;
const byte DIGIT_2 = 0x02;
const byte DIGIT_3 = 0x04;
const byte DIGIT_4 = 0x08;
const byte COLON   = 0x10;

class SevenSegment {
      public:
            int clockPin;
            int latchPin;
            int datoutPin;
            SevenSegment(int clock, int latch, int datout);
            void update();
            void setData(char data[], bool blink = false);
            void setData(int one, int two, bool colon = true, bool blink = false);
            void setDisplayDigit(byte digit, byte segmentData);
            int getDigit(int number, int digit);
            int getLetter(char letter);
            void clearDisplay();
      
      private:
            byte _data[4];
            bool _colon;
            bool _blink;
            bool _blinkOn;
            unsigned long _timer;
            unsigned long _blinkTimer;
            byte _data_index;
            byte _current_digit;
};

#endif