// ---------------------------------------------------------------------------
// Created by Thaddeus Bort, Jr - thaddeusbort@gmail.com
// Copyright 2013 License: GNU GPL v3 http://www.gnu.org/licenses/gpl-3.0.html
//
// See "SevenSegment.h" for purpose, syntax, version history, links, and more.
// ---------------------------------------------------------------------------

#include "SevenSegment.h"

// constants

const long BLINK_TIME = 500000; // in microseconds

//                          A     b     C     d     E     F     g     H     I     J     K     L     M     n     o     P     q     r     S     t     U     v     W     X     y     Z
//const byte alphabet[] = {  'A',  'B',  'C',  'D',  'E',  'F',  'G',  'H',  'I',  'J',  'K',  'L',  'M',  'N',  'O',  'P',  'Q',  'R',  'S',  'T',  'U',  'V',  'W',  'X',  'Y',  'Z' };
//const byte letters[] = { 0x77, 0x7C, 0x39, 0x5E, 0x79, 0x71, 0x6F, 0x76, 0x30, 0x1E, 0x76, 0x38, 0x15, 0x54, 0x5C, 0x73, 0x67, 0x50, 0x6D, 0x78, 0x3E, 0x1C, 0x2A, 0x76, 0x6E, 0x5B };

SevenSegment::SevenSegment(int clock, int latch, int datout) {
  clockPin = clock;
  latchPin = latch;
  datoutPin = datout;

  _timer = 0;
  _blink = _colon = false;
  _data_index = 0;
  _current_digit = DIGIT_1;

  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(datoutPin, OUTPUT);
}
/*
void SevenSegment::setData(char data[], bool blink) {
  for(int i=0; i<4; i++) {
    _data[i] = getLetter(data[i]);
  }
  _colon = false;
  _blink = blink;
}
void SevenSegment::setData(int one, int two, bool colon, bool blink) {
  _data[0] = characters[getDigit(one, 2)];
  _data[1] = characters[getDigit(one, 1)];
  _data[2] = characters[getDigit(two, 2)];
  _data[3] = characters[getDigit(two, 1)];
  _colon = colon;
  _blink = blink;
}

void SevenSegment::setDisplayDigit(byte digit, byte segmentData) {
  /*Serial.print("set digit:");
  Serial.print(digit);
  Serial.print(" data:");
  Serial.println(segmentData);*
  digitalWrite(latchPin, LOW);
  shiftOut(datoutPin, clockPin, MSBFIRST, digit);
  shiftOut(datoutPin, clockPin, MSBFIRST, segmentData);
  digitalWrite(latchPin, HIGH);
}

void SevenSegment::update() {
  // check if any digits need to be updated
  unsigned long micro_time = micros();
  if(micro_time >= _timer && _data[0] > 0) {
    if(_blink) {
      if(micro_time > _blinkTimer) {
        _blinkTimer = micro_time + BLINK_TIME;
        _blinkOn = !_blinkOn;
      }
      if(!_blinkOn) {
        clearDisplay();
        return;
      }
    }
    /*unsigned long elapsed = millis() - blinkTimer;
      if(elapsed > 500) {
        if(elapsed > 1000) {
          _blinkTimer = millis();
        } else if(_blink) {
          clearLCD();
          return;
        }
      } else {
        setLCD(COLON, 0);  // only show colon when not blinking
      }*

    setDisplayDigit(_current_digit, _current_digit < COLON ? _data[_data_index] : 0);
    _timer = micro_time + 2500; // set timer for setting next digit
    _data_index++;
    if((_current_digit <<= 1) > (_colon ? COLON : DIGIT_4))
      _current_digit = DIGIT_1;
      _data_index = 0;
  }
}
*/
void SevenSegment::clearDisplay() {
  setDisplayDigit(0, 0);
  _timer = 0;
  _data[0] = 0;
}


int SevenSegment::getDigit(int number, int digit) {
  return (number / (int)pow(10, digit-1)) % 10;
}