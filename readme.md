NesClock 1.1
 by Thaddeus Bort, Jr
 started on 4/24/2013
 rebooted on 10/15/2013
-----------------------

##todo
 *figure out why some tones/frequencies sound bad
 *figure out how to increase brightness of front red LED (smaller resistor?)
 *figure out how to decrease brightness of 7-segment LEDs (bigger resistors?)

##Inputs/Outputs
 *Inputs
   *Power btn -- 1 digitalIn
   *Reset btn -- 1 digitalIn
   *Controller -- 5V, GND, 2 digitalOut, 1 digitalIn
 *Outputs
   *LED -- 1 digitalOut
   *Buzzer -- 1 PWM pin
   *LCD -- 12 digital outs
   *Clock timer -- 1 PWM and 1 interrupt pin

##Pinout:
 *pin0  --  #serial
 *pin1  --  #serial
 *pin2  -- Clock interrupt
 *pin3  --  #interrupt
 *pin4  -- Power Btn In
 *pin5  -- Reset Btn In
 *pin6  -- Controller Latch Out
 *pin7  -- Controller Clock Out
 *pin8  -- Controller Data In
 *pin9  -- Buzzer Pin 1
 *pin10 -- Buzzer Pin 2
 *pin11 -- Clock PWM Out
 *pin12 -- 
 *pin13 -- LED Out
 *ana0/14  -- LCD Data Out
 *ana1/15  -- LCD Latch Out
 *ana2/16  -- LCD Clock Out
 *ana3/17  -- 
 *ana4/18  -- 
 *ana5/19  -- 
#Shift Register Pinouts:
 *a0 -- Segment A
 *a1 -- Segment B
 *a2 -- Segment C
 *a3 -- Segment D
 *a4 -- Segment E
 *a5 -- Segment F
 *a6 -- Segment G
 *a7 -- Segment H (am/pm)
 *b0 -- Digit 1
 *b1 -- Digit 2
 *b2 -- Digit 3
 *b3 -- Digit 4
 *b4 -- Colon
 *b5 -- 
 *b6 -- 
 *b7 -- 

#Wiring
power button
 red = 5V
 brown = ground

reset button
 orange = 5V (shared with reset)
 yellow = ground

led pins
 orange = 5V (shared with reset)
 white = ground (left pin looking from front)

controller pinout
 brown = ground
 white = 5V
 yellow = serial out
 orange = latch
 red    = clock


NesClock is an application that will run on an arduino housed inside an NES console box and will control a clock.

The clock and alarm will be settable via the original Player 1 Controller.
 Power button = alarm on/off (indicate with LED)
 Reset button = snooze
 Select = cycle through modes (clock/set clock/set alarm/set alarm tone/options/secret mode not available via select button)

Clock mode
 display clock and sound alarm at alarm time
 hold down A to show alarm time
 hold down A and B to show minutes and seconds
Set Clock and Set Alarm modes
 Start = accept changes and return to clock
 up/b    = increase hours
 down    = decrease hours
 right/a = increase minutes
 left    = decrease minutes
Set Alarm Tone mode
 cycle through all songs using buttons, show name of song and play once
 Start   = accept song as alarm tone and return to clock
 up/down = increase/decrease volume (indicate on screen with vol1-volX)
 right/a = next song
 left/b  = previous song
Options mode
 allow the user to configure some options, save settings to eeprom
  24-hr/12-hr mode
  colon blink
  play song every hour
  Start   = accept changes and return to clock
  up/down = toggle value (Y/N)
  right/a = next option
  left/b  = previous option

Secret mode
 not accessable from main navigation (use konami code)
 something cool or maybe multiple cool things
  7 segment game
   use arrows to navigate around the 


Links
-------------
Controller Pins  http://arduino.cc/forum/index.php/topic,8481.0.html
                 http://psmay.com/2011/10/18/the-game-controller-port-deconstructed/
LED Pins         http://img96.imageshack.us/img96/7374/picture35m.png
Power/Reset Pins http://www.redhotscott.co.uk/nespc.html
                 http://www.raspberrypi.org/phpBB3/viewtopic.php?f=78&t=15766

Arduino Clock    http://www.instructables.com/id/TimeDuino-Arduino-based-clock-using-7-segment-dis/step4/Add-resistors-to-your-transistors-Say-that-5-time/

Accurate no RTC  http://www.instructables.com/id/Arduino-Clock-using-Standard-Clock-Display/
                 http://www.instructables.com/id/Make-an-accurate-Arduino-clock-using-only-one-wire/

NES Clock        http://mavrinac.com/index.cgi?page=nesclock


Source Code      https://github.com/mic159/Arduino-Alarm-Clock/blob/master/AlarmClock.pde