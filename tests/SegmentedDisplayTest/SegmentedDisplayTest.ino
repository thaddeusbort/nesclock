#include <math.h>

#define SEG_A  0x01
#define SEG_B  0x02
#define SEG_C  0x04
#define SEG_D  0x08
#define SEG_E  0x10
#define SEG_F  0x20
#define SEG_G  0x40
#define SEG_H  0x80
//#define SEG_DP  A3
#define DIGIT_1  0x01
#define DIGIT_2  0x02
#define DIGIT_3  0x04
#define DIGIT_4  0x08

#define SHIFT_LATCH 15
#define SHIFT_CLOCK 16
#define SHIFT_DATA 14
//                       0     1     2     3     4     5     6     7     8     9
byte characters[] = { 0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F };
int counter = 1000;

void setup() {
  Serial.begin(9600);
  pinMode(SHIFT_LATCH, OUTPUT);
  pinMode(SHIFT_CLOCK, OUTPUT);
  pinMode(SHIFT_DATA, OUTPUT);
}

boolean showedPatterns = 0;

void loop() {
 // Various patterns and tests
if(!showedPatterns) { 
 pattern(DIGIT_1);
 pattern(DIGIT_2);
 pattern(DIGIT_3);
 pattern(DIGIT_4);

 pattern(0xF);
 showedPatterns = 1;
}
 
 
 // count up
 counter = millis()/1000;
// long beginTime = millis();
 setLCD(DIGIT_4, characters[getDigit(counter, 1)]);
 delayMicroseconds(2500);
 setLCD(DIGIT_2, characters[getDigit(counter, 3)]);
 delayMicroseconds(2500);
 setLCD(DIGIT_3, characters[getDigit(counter, 2)]);
 delayMicroseconds(2500);
 setLCD(DIGIT_1, characters[getDigit(counter, 4)]);
  delayMicroseconds(2500);

// clearLCD();
// while( (millis() - beginTime) < 10) ; // wait for 10ms to pass before we paint the display again
// delay(200);
/* for(int i=0; i<10; i++) {
   setLCD(digit, characters[i]);
   delay(300);
   clearLCD();
   delay(200);
 }*/
}

int getDigit(int number, int digit) {
  return (number / (int)pow(10, digit-1)) % 10;
}

void pattern(int digit) {
  // light up each segment at a time of digit 1
 int i = 1;
 while(i <= 0xFF) {
   setLCD(digit, i);
   delay(100);
   i <<= 1;
 }

 delay(200);
 
  // light up each segment at a time of digit 1
 i = 1;
 while(i <= 0xFF) {
   setLCD(digit, i);
   delay(100);
   i = (i << 1) | 1;
 }
 
 delay(200); 
 
 // light up all segments at once and pulse three times
 for(int i = 0; i<3; i++) {
   setLCD(digit, 0xFF);
   delay(200);
   clearLCD();
   delay(200);
 }
}


void setLCD(byte digit, byte segment) {
//    Serial.print("power:");
    Serial.print(digit);
    Serial.println(segment);
    digitalWrite(SHIFT_LATCH, LOW);
    shiftOut(SHIFT_DATA, SHIFT_CLOCK, MSBFIRST, digit);
    shiftOut(SHIFT_DATA, SHIFT_CLOCK, MSBFIRST, segment);
    digitalWrite(SHIFT_LATCH, HIGH);
}
void clearLCD() {
  setLCD(0, 0);
}
