/*
  DigitalReadSerial
 Reads a digital input on pin 2, prints the result to the serial monitor 
 
 This example code is in the public domain.
 */

// digital pin 2 has a pushbutton attached to it. Give it a name:
int pushButton = 4;
int pushButton2 = 5;
int ledPin = 13;
//const int analogInPin = A4;  // Analog input pin that the potentiometer is attached to

int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value output to the PWM (analog out)

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // make the pushbutton's pin an input:
  pinMode(pushButton, INPUT);
//  digitalWrite(pushButton, HIGH);
  pinMode(pushButton2, INPUT);
  pinMode(ledPin, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  
    // read the analog in value:
//  sensorValue = analogRead(analogInPin);            
  // map it to the range of the analog out:
//  outputValue = map(sensorValue, 0, 1023, 0, 255);
  
  // read the input pin:
  int buttonState = digitalRead(pushButton);
  int buttonState2 = digitalRead(pushButton2);
  
  digitalWrite(ledPin, buttonState && !buttonState2);
  
  Serial.print("power:");
  Serial.println(buttonState);
  Serial.print("reset:");
  Serial.println(buttonState2);
  delay(100);        // delay in between reads for stability
}


