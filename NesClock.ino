// An arduino-based alarm clock housed inside an NES and controlled by an NES controller and buttons.
#include <math.h>
#include <EEPROM.h>
#include <NesController.h>
#include <SevenSegment.h>
#include <toneAC_RTTTL.h>
#include <avr/pgmspace.h> // this is needed to store song variables in program memory instead of RAM

const int EEPROM_HOURS            =  0;
const int EEPROM_MINUTES          =  1;
const int EEPROM_ALARM_HOURS      =  2;
const int EEPROM_ALARM_MINUTES    =  3;
const int EEPROM_ALARM_TONE       =  4;
const int EEPROM_VOLUME           =  5;
const int EEPROM_24HR_CLOCK       =  6;
const int EEPROM_COLON_BLINK      =  7;
const int EEPROM_HOUR_TONE_ENABLE =  8;
const int EEPROM_HOUR_TONE        =  9;
const int EEPROM_SNOOZE_LENGTH    = 10;

const int CLOCK_INTERRUPT = 0;
const int INPUT_POWER_BTN               =  4;
const int INPUT_RESET_BTN               =  5;
const int INPUT_NES_CONTROLLER_DATA     =  8;

const int PWM_CLOCK                     = 11;
// these are setup in the ToneAC library and need to be pins 9 and 10
//const int PWM_BUZZER_1                =  9;
//const int PWM_BUZZER_2                = 10;
const int OUTPUT_LED                    = 13;
const int OUTPUT_LCD_SHIFT_LATCH        = 15;
const int OUTPUT_LCD_SHIFT_CLOCK        = 16;
const int OUTPUT_LCD_SHIFT_DATA         = 14;
const int OUTPUT_NES_CONTROLLER_LATCH   =  6;
const int OUTPUT_NES_CONTROLLER_CLOCK   =  7;

const int MSG_DISPLAY_MS = 800;
const int DISPLAY_DELAY_MS = 4;

const byte SECRET_SIZE = 10;
const byte secret[SECRET_SIZE] = { NES_UP, NES_UP, NES_DOWN, NES_DOWN, NES_LEFT, NES_RIGHT, NES_LEFT, NES_RIGHT, NES_B, NES_A };
byte secretCounter = 0;
byte crazyDigitCounter = 0;
byte crazyCounter = '0';
byte crazyArray[4];
unsigned long crazyTimer = 0;

char song_buffer[500];
const int NUM_SONGS = 22;
//prog_char bubble[] PROGMEM = "Bubble Bobble:d=4,o=5,b=125:8a#6,8a6,8g.6,16f6,8a6,8g6,8f6,8d#6,8g6,8f6,16d#6,8d.6,f.6,16d6,16c6,8a#,8c6,8d6,8d#6,8c6,16d6,8d#.6,8f6,8f6,8g6,16a6,8g.6,8f6,8f6,8g6,8a6,8a#6,8a6,8g.6,16f6,8a6,8g6,8f6,8d#6,8g6,8f6,16d#6,8d.6,f.6,16d6,16c6,8a#,8c6,8d6,8d#6,8c6,16d6,8d#.6,8f6,8f6,8g6,16a6,8f.6,8a#.6";

prog_char bubble[] PROGMEM = "Bubble Full:d=4,o=6,b=125:8a#6,8a6,8g.6,16f6,8a6,8g6,8f6,8d#6,8g6,8f6,16d#6,8d.6,f.6,16d6,16c6,8a#5,8c6,8d6,8d#6,8c6,16d6,8d#.6,8f6,8f6,8g6,16a6,8g.6,8f6,8f6,8g6,8a6,8a#6,8a6,8g.6,16f6,8a6,8g6,8f6,8d#6,8g6,8f6,16d#6,8d.6,f.6,16d6,16c6,8a#5,8c6,8d6,8d#6,8c6,16d6,8d#.6,8f6,8f6,8g6,16a6,8f.6,8a#6";
prog_char bubble2[] PROGMEM = "Bubble Full2:d=4,o=6,b=125:8f6,8g6,8g#6,2a6,8f6,8g6,8a6,2a#6,8f6,8g6,8a6,2c7,8f6,8g6,8a#6,2d7,8a#6,8c7,8d7,4d#.7,4d#7,4d7,8c7,2d7,4d7,4c.7,4f.6,4d7,4c.7,8f6,8g6,8g#6,8a6,8f5,8g5,8g#5,8a5,8f6,8g6,8a6,8a#6,8f5,8g5,8a5,8a#5,8f6,8g6,8a6,8c7,8f5,8g5,8a5,8c6,8f6,8g6,8a#6,8d7,8f5,8g5,8a#5,8d6,8a#6,8c7,8d7,4d#.7,4d#7,4d7,8c7,2d7,4d7,4c.7,4f6,8d7,8c7,8d7,2a#6";

//prog_char bubble[] PROGMEM = "Bubble Full:d=4,o=6,b=125:8a#6,8a6,8g.6,16f6,8a6,8g6,8f6,8d#6,8g6,8f6,16d#6,8d.6,f.6,16d6,16c6,8a#5,8c6,8d6,8d#6,8c6,16d6,8d#.6,8f6,8f6,8g6,16a6,8g.6,8f6,8f6,8g6,8a6,8a#6,8a6,8g.6,16f6,8a6,8g6,8f6,8d#6,8g6,8f6,16d#6,8d.6,f.6,16d6,16c6,8a#5,8c6,8d6,8d#6,8c6,16d6,8d#.6,8f6,8f6,8g6,16a6,8f.6,8a#6,8f6,8g6,8g#6,2a6,8f6,8g6,8a6,2a#6,8f6,8g6,8a6,2c7,8f6,8g6,8a#6,2d7,8a#6,8c7,8d7,4d#.7,4d#7,4d7,8c7,2d7,4d7,4c.7,4f.6,4d7,4c.7,8f6,8g6,8g#6,8a6,8f5,8g5,8g#5,8a5,8f6,8g6,8a6,8a#6,8f5,8g5,8a5,8a#5,8f6,8g6,8a6,8c7,8f5,8g5,8a5,8c6,8f6,8g6,8a#6,8d7,8f5,8g5,8a#5,8d6,8a#6,8c7,8d7,4d#.7,4d#7,4d7,8c7,2d7,4d7,4c.7,4f6,8d7,8c7,8d7,2a#6";
//prog_char bubble[] PROGMEM = "bubbleb:d=4,o=5,b=117:8a#,8a,8g.,16f,8a,8g,8f,8d#,8g,8f,16d#,8d.,f.,16d,16c,8a#4,8c,8d,8d#,8c,16d,8d#.,8f,8f,8g,16a,8g.,8f,8f,8g,8a,8a#,8a,8g.,16f,8a,8g,16f,8d#.,8g,8f,16d#,8d.,f.,16d,16c,8a#4,8c,8d,8d#,8c,16d,8d#.,8f,8f,8g,16a,8f.,8a#,8f,8g,8g#,2a,8f,8g,8a,2a#,8f,8g,8a,2c6,8f,8g,8a,2d6,8a#,8c6,8d6,8d#6,d#6,d#6,8d6,c6,2d.6,d6,c.6,g,8g,d6,2c6,8f,8g,8g#,2a,8f,8g,8a,2a#,8f,8g,8a,2c6,8f,8g,8a,2d6,8a#,8c6,8d6,8d#6,d#6,d#6,8d6,c6,2d.6,d6,c.6,f,8d6,8f,8d6,2a#,8f,8g,8a";

prog_char imperial[] PROGMEM = "imperial:d=4,o=5,b=80:8d.,8d.,8d.,8a#4,16f,8d.,8a#4,16f,d.,32p,8a.,8a.,8a.,8a#,16f,8c#.,8a#4,16f,d.,32p,8d.6,8d,16d,8d6,32p,8c#6,16c6,16b,16a#,8b,32p,16d#,8g#,32p,8g,16f#,16f,16e,8f,32p,16a#4,8c#,32p,8a#4,16c#,8f.,8d,16f,a.,32p,8d.6,8d,16d,8d6,32p,8c#6,16c6,16b,16a#,8b,32p,16d#,8g#,32p,8g,16f#,16f,16e,8f,32p,16a#4,8c#,32p,8a#4,16f,8d.,8a#4,16f,d.";
prog_char mario1[] PROGMEM = "Mario:d=4,o=5,b=100:16e6,16e6,32p,8e6,16c6,8e6,8g6,8p,8g,8p,8c6,16p,8g,16p,8e,16p,8a,8b,16a#,8a,16g.,16e6,16g6,8a6,16f6,8g6,8e6,16c6,16d6,8b,16p,8c6,16p,8g,16p,8e,16p,8a,8b,16a#,8a,16g.,16e6,16g6,8a6,16f6,8g6,8e6,16c6,16d6,8b,8p,16g6,16f#6,16f6,16d#6,16p,16e6,16p,16g#,16a,16c6,16p,16a,16c6,16d6,8p,16g6,16f#6,16f6,16d#6,16p,16e6,16p,16c7,16p,16c7,16c7,p,16g6,16f#6,16f6,16d#6,16p,16e6,16p,16g#,16a,16c6,16p,16a,16c6,16d6,8p,16d#6,8p,16d6,8p,16c6";
prog_char tmnt[] PROGMEM = "tmnt:d=4,o=5,b=100:8e,8f#,8e,8f#,8e,16f#,8e.,8f#,8g,8a,8g,8a,8g,16a,8g.,8a,8c6,8d6,8c6,8d6,8c6,16d6,8c.6,8d6,16a,16a,16a,16a,8g,8a,8p,16a,16a,16a,16a";
prog_char simpsons[] PROGMEM = "The Simpsons:d=4,o=5,b=160:c.6,e6,f#6,8a6,g.6,e6,c6,8a,8f#,8f#,8f#,2g,8p,8p,8f#,8f#,8f#,8g,a#.,8c6,8c6,8c6,c6";
prog_char bond[] PROGMEM = "Bond:d=4,o=5,b=80:32p,16c#6,32d#6,32d#6,16d#6,8d#6,16c#6,16c#6,16c#6,16c#6,32e6,32e6,16e6,8e6,16d#6,16d#6,16d#6,16c#6,32d#6,32d#6,16d#6,8d#6,16c#6,16c#6,16c#6,16c#6,32e6,32e6,16e6,8e6,16d#6,16d6,16c#6,16c#7,c.7,16g#6,16f#6,g#.6";
prog_char indy[] PROGMEM = "Indiana:d=4,o=5,b=250:e,8p,8f,8g,8p,1c6,8p.,d,8p,8e,1f,p.,g,8p,8a,8b,8p,1f6,p,a,8p,8b,2c6,2d6,2e6,e,8p,8f,8g,8p,1c6,p,d6,8p,8e6,1f.6,g,8p,8g,e.6,8p,d6,8p,8g,e.6,8p,d6,8p,8g,f.6,8p,e6,8p,8d6,2c6";
prog_char impossible[] PROGMEM = "MissionImp:d=16,o=6,b=95:32d,32d#,32d,32d#,32d,32d#,32d,32d#,32d,32d,32d#,32e,32f,32f#,32g,g,8p,g,8p,a#,p,c7,p,g,8p,g,8p,f,p,f#,p,g,8p,g,8p,a#,p,c7,p,g,8p,g,8p,f,p,f#,p,a#,g,2d,32p,a#,g,2c#,32p,a#,g,2c,a#5,8c,2p,32p,a#5,g5,2f#,32p,a#5,g5,2f,32p,a#5,g5,2e,d#,8d";
prog_char takeonme[] PROGMEM = "TakeOnMe:d=4,o=4,b=160:8f#5,8f#5,8f#5,8d5,8p,8b,8p,8e5,8p,8e5,8p,8e5,8g#5,8g#5,8a5,8b5,8a5,8a5,8a5,8e5,8p,8d5,8p,8f#5,8p,8f#5,8p,8f#5,8e5,8e5,8f#5,8e5,8f#5,8f#5,8f#5,8d5,8p,8b,8p,8e5,8p,8e5,8p,8e5,8g#5,8g#5,8a5,8b5,8a5,8a5,8a5,8e5,8p,8d5,8p,8f#5,8p,8f#5,8p,8f#5,8e5,8e5";
prog_char looney[] PROGMEM = "Looney:d=4,o=5,b=140:32p,c6,8f6,8e6,8d6,8c6,a.,8c6,8f6,8e6,8d6,8d#6,e.6,8e6,8e6,8c6,8d6,8c6,8e6,8c6,8d6,8a,8c6,8g,8a#,8a,8f";
prog_char mario2[] PROGMEM = "Mario_under:d=4,o=6,b=100:32c,32p,32c7,32p,32a5,32p,32a,32p,32a#5,32p,32a#,2p,32c,32p,32c7,32p,32a5,32p,32a,32p,32a#5,32p,32a#,2p,32f5,32p,32f,32p,32d5,32p,32d,32p,32d#5,32p,32d#,2p,32f5,32p,32f,32p,32d5,32p,32d,32p,32d#5,32p,32d#";
prog_char twentieth[] PROGMEM = "20thCenFox:d=16,o=5,b=140:b,8p,b,b,2b,p,c6,32p,b,32p,c6,32p,b,32p,c6,32p,b,8p,b,b,b,32p,b,32p,b,32p,b,32p,b,32p,b,32p,b,32p,g#,32p,a,32p,b,8p,b,b,2b,4p,8e,8g#,8b,1c#6,8f#,8a,8c#6,1e6,8a,8c#6,8e6,1e6,8b,8g#,8a,2b";
prog_char starwars[] PROGMEM = "StarWars:d=4,o=5,b=45:32p,32f#,32f#,32f#,8b.,8f#.6,32e6,32d#6,32c#6,8b.6,16f#.6,32e6,32d#6,32c#6,8b.6,16f#.6,32e6,32d#6,32e6,8c#.6,32f#,32f#,32f#,8b.,8f#.6,32e6,32d#6,32c#6,8b.6,16f#.6,32e6,32d#6,32c#6,8b.6,16f#.6,32e6,32d#6,32e6,8c#6";
prog_char western[] PROGMEM = "GoodBad:d=4,o=5,b=56:32p,32a#,32d#6,32a#,32d#6,8a#.,16f#.,16g#.,d#,32a#,32d#6,32a#,32d#6,8a#.,16f#.,16g#.,c#6,32a#,32d#6,32a#,32d#6,8a#.,16f#.,32f.,32d#.,c#,32a#,32d#6,32a#,32d#6,8a#.,16g#.,d#";
prog_char topgun[] PROGMEM = "TopGun:d=4,o=4,b=31:32p,16c#,16g#,16g#,32f#,32f,32f#,32f,16d#,16d#,32c#,32d#,16f,32d#,32f,16f#,32f,32c#,16f,d#,16c#,16g#,16g#,32f#,32f,32f#,32f,16d#,16d#,32c#,32d#,16f,32d#,32f,16f#,32f,32c#,g#";
prog_char mahnamahna[] PROGMEM = "MahnaMahna:d=16,o=6,b=125:c#,c.,b5,8a#.5,8f.,4g#,a#,g.,4d#,8p,c#,c.,b5,8a#.5,8f.,g#.,8a#.,4g,8p,c#,c.,b5,8a#.5,8f.,4g#,f,g.,8d#.,f,g.,8d#.,f,8g,8d#.,f,8g,d#,8c,a#5,8d#.,8d#.,4d#,8d#.";
prog_char arkanoid[] PROGMEM = "Arkanoid:d=4,o=5,b=140:8g6,16p,16g.6,2a#6,32p,8a6,8g6,8f6,8a6,2g6";
prog_char countdown[] PROGMEM = "TheFinalCountdown:d=4,o=5,b=125:p,8p,16b,16a,b,e,p,8p,16c6,16b,8c6,8b,a,p,8p,16c6,16b,c6,e,p,8p,16a,16g,8a,8g,8f#,8a,g.,16f#,16g,a.,16g,16a,8b,8a,8g,8f#,e,c6,2b.,16b,16c6,16b,16a,1b";
prog_char drmario[] PROGMEM = "dmario:d=4,o=5,b=140:8c6,8p,8c6,8p,8c6,2p,8c#.6,16p,8c#6,8d#6,8p,8d#6,8p,8c6,8p,8c#6,8p,8c6,2p,8c#.6,16p,8c#6,8d#6,8p,8d#6,8p,8c6,8p,8c6,8p,8g#,2p,8c#.6,16p,8c#6,8d#6,8p,8d#6,8p,8c6,8p,8c#6,8p,8c6,2p,8d#6,8c#6,8p,8c6,8p,8a#";
prog_char drchill[] PROGMEM = "dm_chill:d=4,o=5,b=125:8d#6,8b,8d#6,b,p,8g#,8g#,8b,8g#,b,p,8d#6,8b,8d#6,f#6,8f6,8c#6,2p,16d#6,16d#6,8b,8d#6,b,p,8g#,8g#,8b,8g#,d#,p,8d#6,8b,8d#6,f#6,8f6,8c#6,d#6";
prog_char drfever[] PROGMEM = "fever:d=4,o=5,b=125:8g4,8g4,8a#4,8b4,8c,8b4,8a#4,8a4,8g4,8g4,8a#4,8b4,8c,8b4,8a#4,8a4,8a#,8b,8a#,8b,8a,8g,8g,8a,8a#,8b,8a,8g,8g,p,8p,8a#,8b,8a#,8b,8a,8g,8g,8a,8b4,8p,8c,8p,8c#,8p,8d";

PROGMEM const char *songs[] = {
  bubble, bubble2, mario1, tmnt, simpsons, bond, indy, impossible, takeonme, looney
, mario2, twentieth, starwars, western, topgun, mahnamahna, arkanoid, countdown, drmario, drchill
, drfever, imperial
};
int eep_volume = 1;
int volume = 1;
int preview_index = 0;
bool song_started = false;
char song_name[4];


enum Modes {
  Clock,
  SetClock,
  SetAlarm,
  SetAlarmTone,
  Settings,
  Secret
};

int masterClock = 0;  // counts rising edge clock signals
byte seconds = 0;
byte minutes = 0;
byte hours = 0;

unsigned long timeMs;
unsigned long blinkTimer;
unsigned long volumeTimer;

bool alarmEnabled = false;
byte alarmMinutes = 0;
byte alarmHours = 0;
byte alarmTone = 0;

bool hourToneEnabled = true;
byte hourTone = 0;

Modes mode = Clock;
Modes lastMode = Clock;
unsigned long modeChange = 0;

bool alarmSounding = false;
bool hourToneSounding = false;
bool alarmStopped = false;
bool alarmSnooze = false;

NesController nesController(OUTPUT_NES_CONTROLLER_CLOCK, OUTPUT_NES_CONTROLLER_LATCH, INPUT_NES_CONTROLLER_DATA);
SevenSegment display(OUTPUT_LCD_SHIFT_CLOCK, OUTPUT_LCD_SHIFT_LATCH, OUTPUT_LCD_SHIFT_DATA);

void setup() {
  Serial.begin(9600);
  pinMode(INPUT_POWER_BTN, INPUT);
  pinMode(INPUT_RESET_BTN, INPUT);
  pinMode(PWM_CLOCK, OUTPUT);
  pinMode(OUTPUT_LED, OUTPUT);

  hours = getEeprom(EEPROM_HOURS, 0);
  minutes = getEeprom(EEPROM_MINUTES, 0);
  alarmHours = getEeprom(EEPROM_ALARM_HOURS, 0);
  alarmMinutes = getEeprom(EEPROM_ALARM_MINUTES, 0);
  alarmTone = getEeprom(EEPROM_ALARM_TONE, 0);
  eep_volume = volume = getEeprom(EEPROM_VOLUME, 1);

  hourToneEnabled = getEeprom(EEPROM_HOUR_TONE_ENABLE);
  hourTone = getEeprom(EEPROM_HOUR_TONE);

  // setup clock interrupt
  attachInterrupt(CLOCK_INTERRUPT, clockInterrupt, RISING);
  analogWrite(PWM_CLOCK, 127);  // this starts our PWM 'clock' with a 50% duty cycle
}

byte getEeprom(int address, byte defaultVal) {
  byte val = EEPROM.read(address);
  Serial.print("Value: ");
  Serial.print(val);
  Serial.print(" read at address: ");
  Serial.println(address);
  if(val == 0xFF)
    return defaultVal;
  return val;
}

void clockInterrupt() {
  // increment seconds based on the PWM from the interrupt
  masterClock++;  // with each clock rise add 1 to masterclock count
  if(masterClock == 489) { // 490Hz reached
    seconds++;        // after one 490Hz cycle add 1 second
    masterClock = 0;  // reset after 1 second is reached
    if(seconds == 60) {
      seconds = 0;
      minutes++;
      if(minutes == 60) {
        minutes = 0;
        hours++;
        if(hours == 24) {
          hours = 0;
        }
      }
/*      Serial.print("Time: ");
      Serial.print(hours);
      Serial.print(":");
      Serial.print(minutes);
      Serial.print(":");
      Serial.println(seconds);*/
    }
  }
}

void loop() {
  timeMs = millis();
  // check controller input
  nesController.update();
  //display.update();

  // konami code checker
  if(nesController.isPressed(secret[secretCounter])) {
    secretCounter++;
    //Serial.print("Secret counter: ");
    //Serial.println(secretCounter);
    if(secretCounter == SECRET_SIZE) {
      mode = Secret;
      secretCounter = 0;
    }
  } else if(nesController.isAnyButtonPressed()) {
    // if a button was pressed that's not in the konami code sequence then reset the secret counter
    secretCounter = 0;
  }

  // select button cycles through screens, start returns to clock
  if(nesController.isPressed(NES_SELECT) || nesController.isPressed(NES_START)) {
    if(nesController.isPressed(NES_SELECT)) {
      mode = static_cast<Modes>(static_cast<int>(mode) + 1);
      modeChange = timeMs;
      // don't allow getting to secret mode directly
      if(mode > Settings)
        mode = Clock;
    } else {
      mode = Clock;
    }

    if(lastMode == SetAlarm) {
      EEPROM.write(EEPROM_ALARM_HOURS, alarmHours);
      EEPROM.write(EEPROM_ALARM_MINUTES, alarmMinutes);
    } else if(lastMode == SetAlarmTone) {
      stopSong();
      if(alarmTone != preview_index) {
        alarmTone = preview_index;
        EEPROM.write(EEPROM_ALARM_TONE, alarmTone);
      }
      if(volume != eep_volume) {
        eep_volume = volume;
        EEPROM.write(EEPROM_VOLUME, eep_volume);
      }
    }

    //Serial.print("Mode: ");
    //Serial.println(mode);
  }

  // check button inputs
  if(!alarmSnooze && digitalRead(INPUT_RESET_BTN) && !alarmSounding) {
    // pushed reset button -- save clock
    EEPROM.write(EEPROM_HOURS, hours);
    EEPROM.write(EEPROM_MINUTES, minutes);
  }
  alarmEnabled = digitalRead(INPUT_POWER_BTN);
  digitalWrite(OUTPUT_LED, alarmEnabled);
  alarmSnooze = digitalRead(INPUT_RESET_BTN);

  if(mode == Clock && alarmEnabled && !alarmStopped && !alarmSounding && !hourToneSounding) {
    if(hours == alarmHours && minutes == alarmMinutes) {  // play tone on alarm
      alarmSounding = true;
      strcpy_P(song_buffer, (char*)pgm_read_word(&(songs[alarmTone])));
      toneAC_RTTTL(song_buffer, volume, true);
    } else if(minutes == 0) { // play a different tone every hour on the hour
      hourToneSounding = true;
      strcpy_P(song_buffer, (char*)pgm_read_word(&(songs[hourTone])));
      toneAC_RTTTL(song_buffer, volume, true);
      if(++hourTone >= NUM_SONGS)
        hourTone = 0;
      EEPROM.write(EEPROM_HOUR_TONE, hourTone); // save the current hour tone
    }
  }
  
  //if(!alarmSounding && alarmEnabled && hours == alarmHours && minutes == alarmMinutes)
  //  alarmSounding = true;
  //else if(alarmSounding) {
  if(alarmSounding || hourToneSounding) {
    if(!alarmEnabled || alarmSnooze) {
      // turn off alarm or snooze
      alarmSounding = hourToneSounding = false;
      alarmStopped = true;
      stopSong();
    } else if(!isSongPlaying() && !alarmStopped && alarmSounding) {
      // start alarm or start song again if stopped
      //strcpy_P(song_buffer, (char*)pgm_read_word(&(songs[alarmSounding ? alarmTone : hourTone])));
      toneAC_RTTTL(song_buffer, volume, true);
    }
  }

  if(mode == Clock || mode == SetClock) {
    // display clock
    if(mode == Clock && nesController.isDown(NES_A)) {
      if(nesController.isDown(NES_B))
        showTime(minutes, seconds, false); // hold down A and B to see seconds
      else
        showTime(alarmHours, alarmMinutes, false); // hold down A to see alarm
    } else
      showTime(hours, minutes, mode == SetClock);
    if(mode == SetClock) {
      // allow changing time
      setTime(hours, minutes);
    }
  } else if(mode == SetAlarm) {
    if(timeMs - modeChange < MSG_DISPLAY_MS) {
      showMessage((char*)"ALRM");
    } else {
      // blink alarm time and allow changing
      showTime(alarmHours, alarmMinutes, true);
      setTime(alarmHours, alarmMinutes);
    }
  } else if(mode == SetAlarmTone) {
    if(timeMs - modeChange < MSG_DISPLAY_MS) {
      showMessage((char*)"TONE");
      preview_index = alarmTone;
    } else {
      // show short name for each song and go through list of all songs/tones
      if(nesController.isPressed(NES_UP) && volume < 10) {
        setVolume(++volume);
        volumeTimer = timeMs + MSG_DISPLAY_MS;
      } else if(nesController.isPressed(NES_DOWN) && volume > 1) {
        setVolume(--volume);
        volumeTimer = timeMs + MSG_DISPLAY_MS;
      } else if(nesController.isPressed(NES_RIGHT) || nesController.isPressed(NES_A)) {
        stopSong(); // stop song if playing
        if(++preview_index >= NUM_SONGS)
          preview_index = 0;
        song_started = false;
      } else if(nesController.isPressed(NES_LEFT) || nesController.isPressed(NES_B)) {
        stopSong(); // stop song if playing
        if(--preview_index < 0)
          preview_index = NUM_SONGS-1;
        song_started = false;
      }
      
      if(!song_started) {
        strcpy_P(song_buffer, (char*)pgm_read_word(&(songs[preview_index])));
        toneAC_RTTTL(song_buffer, volume, true);
        song_started = true;
        /*char *name = getSongName();
        for(int i=0; i<4; i++) {
          if(name[i] >= 'a' && name[i] <= 'z')
            song_name[i] = name[i] - 0x20; // change lowercase to uppercase to match alphabet array
          else if(name[i] >= 'A' && name[i] <= 'Z')
            song_name[i] = name[i];
        }*/
      }

      // TODO: display song name
      //showMessage(song_name);
      if(volumeTimer > timeMs)
        showVolume();
      else {
        showNum(preview_index+1);
      }
    }
  } else if(mode == Settings) {
    if(timeMs - modeChange < MSG_DISPLAY_MS) {
      showMessage((char*)"CONF");
    } else {
      // TODO: add an actual settings mode
      mode = Clock;
    }
  } else if(mode == Secret) {
    // TODO: make the display go crazy first and then play a game or something neat

    //setLCD(random(COLON), random(256) & ~128); // & ~128 is to preven the decimal point from showing up because I don't like the way it looks
    // crazy random letters
    //setLCD(random(COLON), segment_ascii[random(27)]);


    // crazy slots game
    for(int i=0; i<4; i++) {
      if(crazyArray[i] != 0) {
        setLCD(1 << i, segment_ascii[crazyArray[i]]);
        delay(DISPLAY_DELAY_MS);
      }
    }
    if(crazyDigitCounter < 4) {
      setLCD(1 << crazyDigitCounter, segment_ascii[crazyCounter]);
      if(nesController.isAnyButtonPressed() && lastMode == Secret) {
        crazyArray[crazyDigitCounter] = crazyCounter;
        crazyDigitCounter++;
      }
      if(timeMs > crazyTimer) {
        crazyTimer = timeMs + 50;
        crazyCounter++;
        if(crazyCounter >= '9' && crazyCounter < 'A')
          crazyCounter = 'A';
        else if(crazyCounter >= 'Z' && crazyCounter < 'a')
          crazyCounter = 'a';
        else if(crazyCounter >= 'z')
          crazyCounter = '0';

      }
      delay(DISPLAY_DELAY_MS);
    } else if(nesController.isAnyButtonPressed()) {
      crazyDigitCounter = 0;
      crazyArray[0] = crazyArray[1] = crazyArray[2] = crazyArray[3] = 0;
    }

    //setLCD(crazyDigitCounter, crazyCounter & ~128); // & ~128 is to preven the decimal point from showing up because I don't like the way it looks
    //if(++crazyDigitCounter > COLON)
    //  crazyDigitCounter = 0;
    //if(++crazyCounter > 0xFF)
    //  crazyCounter = 0;
    //delay(50)
  }
  lastMode = mode;
}

void setTime(byte &hrs, byte &mins) {
  if(nesController.isPressed(NES_UP)) {
      if(++hrs > 23)
        hrs = 0;
    } else if(nesController.isPressed(NES_DOWN)) {
      if(--hrs < 0)
        hrs = 23;
    } else if(nesController.isPressed(NES_A)) {
      if(++mins > 59)
        mins = 0;
    } else if(nesController.isPressed(NES_B)) {
      if(--mins < 0)
        mins = 59;
    }
}

void showTime(int hrs, int mins, bool blink) {
  if(masterClock < 245) {
    // clear colon (or full display if blink is true) on the top half of each second
    if(blink) {
      clearLCD();
      return;
    }
  } else {
    setLCD(COLON, 0);  // only show colon when not blinking
  }
  if(hrs >= 10) {
    setLCD(DIGIT_1, segment_ascii[getDigit(hrs, 2)]);
    delay(DISPLAY_DELAY_MS);
  }
  setLCD(DIGIT_2, segment_ascii[getDigit(hrs, 1)]);
  delay(DISPLAY_DELAY_MS);
  setLCD(DIGIT_3, segment_ascii[getDigit(mins, 2)]);
  delay(DISPLAY_DELAY_MS);
  setLCD(DIGIT_4, segment_ascii[getDigit(mins, 1)]);
  delay(DISPLAY_DELAY_MS);
}

void showNum(int num) {
  if(num >= 10) {
    setLCD(DIGIT_3, segment_ascii[getDigit(num, 2)]);
    delay(DISPLAY_DELAY_MS);
  }
  setLCD(DIGIT_4, segment_ascii[getDigit(num, 1)]);
  delay(DISPLAY_DELAY_MS);
}
void showVolume() {
  setLCD(DIGIT_1, segment_ascii['v']);
  delay(DISPLAY_DELAY_MS);
  setLCD(DIGIT_2, segment_ascii['o']);
  delay(DISPLAY_DELAY_MS);
  setLCD(DIGIT_3, segment_ascii['l']);
  delay(DISPLAY_DELAY_MS);
  setLCD(DIGIT_4, segment_ascii[volume >= 9 ? 9 : volume]);
  delay(DISPLAY_DELAY_MS);
}

int getDigit(int number, int digit) {
  return (number / (int)pow(10, digit-1)) % 10;
}

void showMessage(char message[4]) {
  setLCD(DIGIT_1, segment_ascii[message[0]]);
  delay(DISPLAY_DELAY_MS);
  setLCD(DIGIT_2, segment_ascii[message[1]]);
  delay(DISPLAY_DELAY_MS);
  setLCD(DIGIT_3, segment_ascii[message[2]]);
  delay(DISPLAY_DELAY_MS);
  setLCD(DIGIT_4, segment_ascii[message[3]]);
  delay(DISPLAY_DELAY_MS);
}

void setLCD(byte digit, byte segment) {
  digitalWrite(OUTPUT_LCD_SHIFT_LATCH, LOW);
  shiftOut(OUTPUT_LCD_SHIFT_DATA, OUTPUT_LCD_SHIFT_CLOCK, MSBFIRST, digit);
  shiftOut(OUTPUT_LCD_SHIFT_DATA, OUTPUT_LCD_SHIFT_CLOCK, MSBFIRST, segment);
  digitalWrite(OUTPUT_LCD_SHIFT_LATCH, HIGH);
}
void clearLCD() {
  setLCD(0, 0);
}
